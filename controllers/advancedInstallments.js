const model = require("../models/advancedInstallments");
const methods = require("../libs/methodsClass");

const controller = {};

const cbkSuccessGetDocuments = param => param;
const cbkSuccessSaveDocument = param => {
  const response = {
    method: "POST",
    param
  };

  return response;
};

const cbkSuccessDeleteDocument = param => `Elimino un documento: ${param}`;

const cbkErrorGetDocuments = code => getDocuments[code];

const cbkSuccessFindDocument = (response, params) => {
  if (response.length == 0) {
    return response;
  } else {
    return (newresponse = response.map(ele => {
      let newElement = JSON.parse(JSON.stringify(ele));
      delete newElement.userId;
      return newElement;
    }));
  }
};

const getDocuments = (req, res) => {
  return methodsClass.getDocuments({
    res,
    req,
    cbkSuccess: cbkSuccessGetDocuments,
    cbkError: cbkErrorGetDocuments
  });
};

const getDocumentsIo = params => {
  const id = params.req.body._idMortgageLoans ? params.req.body._idMortgageLoans : params.req.params._idMortgageLoans ? params.req.params._idMortgageLoans : params.req.params.id;

  return methodsClass.getDocumentsIo({
    documentToFind: { _idMortgageLoans: id},
    cbkSuccess: cbkSuccessFindDocument
  });
};

controller.findDocuments = (req, res) => {
  const { params, user } = req;
  return methodsClass.findDocument({
    documentToFind: { _idMortgageLoans: params.id },
    res,
    req,
    cbkSuccess: cbkSuccessFindDocument
  });
};

controller.updateDocument = (req, res) => {
  console.log("Entro para modificar");
  const { params, body } = req;
  return methodsClass.updateDocument({
    filter: { _id: params.id },
    document: body,
    res,
    req
  });
};


controller.saveDocument = (req, res) => {
  console.log("POST");
  const { body } = req;
  const payload = body;

  console.log(payload);
  return methodsClass.saveDocument({
    document: payload,
    res,
    req,
    cbkSuccess: cbkSuccessSaveDocument
  });
};

controller.deleteDocument = (req, res) => {
  console.log("Entro para eliminar");
  const { params } = req;
  return methodsClass.deleteDocument({
    condition: { _id: params.id },
    res,
    req,
    cbkSuccess: cbkSuccessDeleteDocument,
    params
  });
};

controller.getDocuments = getDocuments;

const methodsClass = new methods({
  model,
  io: (params) => `advancedInstallments-${params.req.user.email}`,
  cbkIo: getDocumentsIo
});

module.exports = controller;
