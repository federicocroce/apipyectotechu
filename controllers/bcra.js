const { getAxios } = require("../libs/axios.js");

const model = require("../models/bcra");

const methods = require("../libs/methodsClass");

const beginDate = "2016-03-31";

const currentDate = new Date();

const controller = {};

const url = "https://api.estadisticasbcra.com";
const headers = {
  name: "BCRA",
  Authorization:
    "BEARER eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTI1OTE5MzUsInR5cGUiOiJleHRlcm5hbCIsInVzZXIiOiJuaWNvYzEyM0BnbWFpbC5jb20ifQ.ViHrl8doVKT-_vF5OkQdIyB1BI376-sFJzk1rEZP97xwg9BQ4ppDir4AOfJzEps7tLauNw62PMA5aQSerPClng"
  // Authorization:
  //   "BEARER eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTg2Mzc4MjQsInR5cGUiOiJleHRlcm5hbCIsInVzZXIiOiJmZWRlLmNyb2NlLjEyM0BnbWFpbC5jb20ifQ.egn-tp5h13KR0WpgLLrSShDeJs9BEDn0bx-TkvU767NHhGZAk5-Mm_oidXgpHYaCgC30Gkj2laCGSSugOzKxmQ"
};

var delay = 60 * 60 * 1000; // 1 hour in msec

(getCurrency = () => {
  const getCurrencyBCRA = () => {
    console.log("Entra una vez");
    getAxios(`${url}/uva`, {
      headers,
      cbkSuccess: data => cbkSuccess(data, "uva")
    });
    getAxios(`${url}/usd`, {
      headers,
      cbkSuccess: data => cbkSuccess(data, "usd")
    });
  };
  getCurrencyBCRA();
  setInterval(getCurrencyBCRA, delay);
})();

const cbkSuccess = (data, currency) => {
  return methodsClass.updateDocument({
    filter: { _name: currency },
    document: { data, lastUpdate: new Date() }
  });
};

const monthName = {
  "01": "Enero",
  "02": "Febero",
  "03": "Marzo",
  "04": "Abril",
  "05": "Mayo",
  "06": "Junio",
  "07": "Julio",
  "08": "Agosto",
  "09": "Septiembre",
  "10": "Octubre",
  "11": "Noviembre",
  "12": "Diciembre"
};

const cbkSuccessFindDocument = (response, currency, dateSelectedParse) => {
  if (currency == "uva") {
    return formatData(response[0], dateSelectedParse, "UVA");
  } else {
    return formatData(response[0], dateSelectedParse, "USD");
  }
};

const addZeroToNumber = number =>
  number.toString().length == 1 ? "0" + number : number;

controller.getDocumentsAxios = currency => (req, res) => {
  getAxios(`${url}/${currency}`, {
    headers,
    res
  });
};

controller.getDocuments = currency => (req, res) => {
  const dateSelected = !req.query.date
    ? `${currentDate.getFullYear()}-${addZeroToNumber(
        currentDate.getMonth() + 1
      )}-${addZeroToNumber(currentDate.getDate())}`
    : req.query.date;

  const dateSelectedParse = getParseDate(dateSelected);

  methodsClass.findDocument({
    documentToFind: { _name: currency },
    res,
    req,
    cbkSuccess: response =>
      cbkSuccessFindDocument(response, currency, dateSelectedParse)
  });
};

const getDateFormated = date =>
  `${date.getFullYear()}-${addZeroToNumber(
    date.getMonth() + 1
  )}-${addZeroToNumber(date.getDate())}`;

const getParseDate = date => {
  const parseDate = date.split("-");
  return {
    year: addZeroToNumber(parseDate[0]),
    month: addZeroToNumber(parseDate[1]),
    day: addZeroToNumber(parseDate[2]),
    fullDate: function() {
      return `${this.year}, ${this.month}, ${this.day}`;
    }
  };
};

const getLastDay = date => {
  return new Date(date.year, date.month, 0).getDate();
};

const filterDate = (response, dateSelectedParse) => {
  const fromBeginDate = response.filter(element => element.d >= beginDate);
  const firstDate = dateSelectedParse;
  const lastDate = getParseDate(fromBeginDate[fromBeginDate.length - 1].d);

  return fromBeginDate.filter(date => {
    const dateElement = getParseDate(date.d);

    const newFilter =
      dateElement.year == firstDate.year &&
      dateElement.month == firstDate.month;

    return newFilter;
  });
};

const formatData = (response, dateSelectedParse, currencyName) => {
  let newResult = {};
  const currencyHistory = response.data;

  const filterResult = filterDate(currencyHistory, dateSelectedParse);
  const completeFilter = [...filterResult];

  if (currencyName == "USD") {
    let indexFilterResult = 0;

    const lastDay = getLastDay(dateSelectedParse);

    for (let indexDay = 1; indexDay <= lastDay; indexDay++) {
      if (!filterResult[indexFilterResult]) continue;

      const currentDate = getParseDate(filterResult[indexFilterResult].d);
      const currentValue =
        filterResult[
          indexFilterResult == 0 ? indexFilterResult : indexFilterResult - 1
        ].v;
      const currentDayInt = parseInt(currentDate.day);

      while (indexDay != currentDayInt) {
        completeFilter.splice(indexDay - 1, 0, {
          d: `${currentDate.year}-${currentDate.month}-${addZeroToNumber(
            indexDay
          )}`,
          v: currentValue
        });

        indexDay++;
      }
      indexFilterResult++;
    }
  }

  const average = completeFilter.reduce((initialValue, currentValue) => {
    return initialValue + currentValue["v"];
  }, 0);

  const currentMonth = monthName[dateSelectedParse.month];

  const filter = {
    currencyName,
    average: Number((average / completeFilter.length).toFixed(3)),
    month: currentMonth,
    history: completeFilter,
    lastUpdate: response.lastUpdate
  };

  Object.assign(newResult, { ...filter });

  return newResult;
};

const methodsClass = new methods({
  model
});

module.exports = controller;
