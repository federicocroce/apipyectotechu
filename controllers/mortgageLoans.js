const model = require("../models/mortgageLoans");

const methods = require("../libs/methodsClass");

const controller = {};

const errors = {
  getDocuments: {
    "500": "Esto es un error generico"
  },
  getDocument: {
    "404": "el documento no existe"
  }
};

const cbkSuccessGetDocuments = param => param;

const cbkSuccessSaveDocument = param => {
  const response = {
    method: "POST",
    param
  };

  return response;
};
const cbkSuccessUpdateDocument = param => `Actualizo un documento: ${param}`;
const cbkSuccessDeleteDocument = param => `Elimino un documento: ${param}`;

const cbkErrorGetDocuments = code => getDocuments[code];

const cbkSuccessFindDocument = (response, params) => {
  if (response.length == 0) {
    return response;
  } else {
    return (newresponse = response.map(ele => {
      let newElement = JSON.parse(JSON.stringify(ele));
      delete newElement.userId;
      return newElement;
    }));
  }
};

const getDocuments = (req, res) => {
  return methodsClass.getDocuments({
    res,
    req,
    cbkSuccess: cbkSuccessGetDocuments,
    cbkError: cbkErrorGetDocuments
  });
};

const getDocumentsIo = params => {
  return methodsClass.getDocumentsIo({
    documentToFind: { _userId: params.req.user._id },
    cbkSuccess: cbkSuccessFindDocument
  });
};

controller.findDocuments = (req, res) => {
  const { params, user } = req;
  return methodsClass.findDocument({
    documentToFind: { _userId: user._id },
    res,
    req,
    cbkSuccess: cbkSuccessFindDocument
  });
};

controller.updateDocument = (req, res) => {
  console.log("Entro para modificar");
  const { params, body } = req;
  return methodsClass.updateDocument({
    filter: {_id: params.id},
    document: {data: body},
    res,
    req
  });
};

controller.saveDocument = (req, res) => {
  console.log("POST");
  const { body, user } = req;
  const payload = { ...body, ...{ _userId: user._id } };

  console.log(payload);
  return methodsClass.saveDocument({
    document: payload,
    res,
    req,
    cbkSuccess: cbkSuccessSaveDocument
  });
};

controller.deleteDocument = (req, res) => {
  console.log("Entro para eliminar");
  const { params } = req;
  return methodsClass.deleteDocument({
    condition: { _id: params.id },
    res,
    req,
    cbkSuccess: cbkSuccessDeleteDocument,
    params
  });
};

controller.getDocuments = getDocuments;

const methodsClass = new methods({
  model,
  io: (params) => `mortgageLoans-${params.req.user.email}`,
  cbkIo: getDocumentsIo
});

module.exports = controller;
