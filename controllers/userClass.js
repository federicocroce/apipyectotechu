"use strict";

const model = require("../models/user");
var nodemailer = require("nodemailer");
const methods = require("../libs/methodsClass");


const controller = {};

const methodsClass = new methods({
  model
});

const cbkSuccessFindDocument = (users, params) => {
  if (users.length == 0) {
    throw new Error("Usuario no encontrado");
  } else {
    return users[0].comparePassword(params.body.password);
  }
};

controller.deleteDocument = ({ params }, res) => {
  console.log("Entro para eliminar");
  methodsClass.deleteDocument({ id: params.id, res });
};

const cbkSuccessSaveDocument = param => {
  sendMail(param);

  return { message: "El usuario se ha creado correctamente" };
};

const sendMail = param => {
  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "creditlifeapp@gmail.com",
      pass: "fcroce4562"
    }
  });

  var mailOptions = {
    from: "creditlifeapp@gmail.com",
    to: param.email,
    subject: "Registro CreditLife",
    text: "Gracias por registrarse en CreditLife!",
    html: ` <body style="margin:0%; padding:0%;">
    <header
      style="text-align: left; background: rgb(38, 65, 103); padding:10px 5%"
    >
      <div style="    text-align: center; width: 113px;">
        <img style="width: 40px;" src="cid:creditLife" />
        <h1 style="font-size: 1.5em; color: #c9c9c9; margin: 0;">
          CreditLife
        </h1>
      </div>
    </header>
    <div style="padding:2% 5%;width: 80%; margin: 0 auto;">
      <h2>Gracias por registrarse en CreditLife!</h2>
      <p>
        Controle sus adelantos, estime y cancele su crédito hipotecario lo antes
        posible.
      </p>

      <a
        href="http://54.166.151.221:3001/#/login"
        target="_blank"
        style="margin-top: 36px;
      color: #c9c9c9;
      background: rgb(38, 65, 103);
      padding: 15px 30px;
      display: block;
      text-align: center;
      width: 150px;
      text-decoration: none;"
        >CreditLifeApp</a
      >
    </div>
  </body>`,
    attachments: [
      {
        filename: "creditLife.png",
        path: "creditLife.png",
        cid: "creditLife" //same cid value as in the html img src
      }
    ]
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};

controller.getDocuments = (req, res) => {
  console.log("Obetengo productos");
  methodsClass.getDocuments({ res });
};

controller.signUp = ({ body }, res) => {
  const user = {
    email: body.email,
    username: body.username,
    password: body.password
  };

  methodsClass.saveDocument({
    document: user,
    res,
    cbkSuccess: cbkSuccessSaveDocument,
    cbkError: error => {
      const errors = {
        11000: "El usuario ya existe"
      };
      return { message: errors[error.code] };
    }
  });
};

controller.signIn = (req, res) =>
  methodsClass.findDocument({
    documentToFind: { email: req.body.email },
    res,
    cbkSuccess: cbkSuccessFindDocument,
    body: req.body
  });

module.exports = controller;
