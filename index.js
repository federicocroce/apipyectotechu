const express = require('express');
const consign = require('consign');

console.log(process.env.NODE_ENV)

const app = express();

consign()
  .include('libs/middlewares.js')
  .then('controllers')
  .then('routes')  
  .then('libs/boots.js')
  .into(app);
