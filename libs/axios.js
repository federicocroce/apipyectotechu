const axios = require("axios");

module.exports.getAxios = (url, { headers = null, cbkSuccess, res }) => {
  console.log(url);
  axios.get(url, headers && { headers }).then(
    response => {
      cbkSuccess && cbkSuccess(response.data);
      // console.log(response.data);
      res &&
        res.send({
          response: response.data
        });
      return response.data;
    },
    error => {
      console.log(error);
      res && res.status(500).send(error);
      return error;
    }
  );
};
