const mongoose = require("mongoose");
const soketIO = require("socket.io");
mongoose.set("useFindAndModify", false);

const portMongo = process.env.PORT_MONGO || 27017;

// const url = `mongodb://mongo:${portMongo}/creditLife`;
const url =
  process.env.NODE_ENV == "docker"
    ? `mongodb://mongo:${portMongo}/creditLife`
    : `mongodb://localhost:${portMongo}/creditLife`;

console.log(url);

mongoose
  .connect(url, { useNewUrlParser: true })
  .then(() => {
    console.log(
      `Conexion de BD establecida en el puerto: ${portMongo} -  path: ${url}`
    );
  })
  .catch(error => console.log(`Error al conectar con la BD ${error}`));

module.exports = app => {

  const server = app.listen(app.get("port"), () => {
    console.log(`server on port ${app.get("port")}`);
  });

  const io = soketIO(server);

  global.io = io;
  
  global.io.on("connection", function(socket) {
    console.log("Un cliente se ha conectado");
    socket.emit("messages", "messages");
  });



};

