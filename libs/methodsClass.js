const mongoose = require("mongoose");

class methodsClass {
  constructor({ model, io, cbkIo }) {
    this.model = model;
    this.io = io;
    this.cbkIo = cbkIo;
  }
  async _invokeSocketIO(params, hasIo) {
    if (hasIo && this.io && this.cbkIo) {
      try {
        let response = await this.cbkIo(params);
        console.log(response);
        global.io.emit(`${this.io(params)}`, response);
      } catch (error) {
        throw new Error(error);
      }
    }
  }

  _resolveMethod(method, params) {
    const { res, cbkSuccess, cbkError, hasIo = true } = params;
    return (async () => {
      try {
        let response = await method;
        // console.log("Entra al then");
        // console.log(response);

        const result = cbkSuccess ? cbkSuccess(response, params) : response;

        this._invokeSocketIO(params, hasIo);

        res && res.status(200).send({ result });
        return result;
      } catch (error) {
        console.log("Entra al error");

        const result = cbkError
          ? cbkError(error)
          : error.message
          ? { message: error.message }
          : { message: error };

        console.log(result);
        res && res.status(500).send(result);
        return result;
      }
    })();
  }

  getDocuments({ ...params }) {
    return this._resolveMethod(this.model.find({}), {
      ...params,
      hasIo: false
    });
  }

  getDocumentsIo({ ...params }) {
    return this.model.find(params.documentToFind);
  }

  getDocument({ ...params }) {
    return this._resolveMethod(this.model.findById(params.id), {
      ...params,
      hasIo: false
    });
  }

  findDocument({ ...params }) {
    return this._resolveMethod(this.model.find(params.documentToFind), params);
  }

  saveDocument({ ...params }) {
    let newModel = new this.model();
    Object.assign(newModel, params.document);
    return this._resolveMethod(newModel.save(), params);
  }

  updateDocument({ ...params }) {
    return this._resolveMethod(
      this.model.findOneAndUpdate(params.filter, params.document, {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
      }),
      params
    );
  }

  deleteDocument({ ...params }) {
    return this._resolveMethod(
      this.model.findOneAndRemove(params.condition),
      params
    );
  }
}

module.exports = methodsClass;