const bodyParser = require('body-parser');

module.exports = app => {
  app.set('json spaces', 4);
  app.set('port', process.env.PORT || 3000);

  // Para interprestar JSON
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: false}));

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Methods", "*");
    next();
  }) 

};
