"use strict";

const jwt = require("jwt-simple");
const moment = require("moment");
const config = require("./config");

const createToken = user => {
  const payload = {
    sub: {
      _id:user._id,
      email: user.email,
      test: "test"
    },
    iat: moment().unix,
    exp: moment()
      .add(14, "days")
      .unix()
  };

  console.log(payload);

  const token = jwt.encode(payload, config.SECRET_TOKEN);

  // console.log(token);

  return token;
};

const decodeToken = token => {
  const decode = new Promise((resolve, reject) => {
    try {
      let payload = jwt.decode(token, config.SECRET_TOKEN);

      console.log(payload);

      if (payload.exp <= moment().unix())
        resolve({
          status: 401,
          message: "El token expiro"
        });

      resolve(payload.sub);
    } catch (error) {
      console.log(error);
      reject({
        status: 500,
        message: "Invalid token"
      });
    }
  });

  return decode;
};

module.exports = {
  createToken,
  decodeToken
};
