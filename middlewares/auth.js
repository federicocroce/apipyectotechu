'use strict'

const services = require('../libs/services')

function isAuth(req, res, next) {
    console.log("isAuth");
    if (!req.headers.authorization) {
        return res.status(403).send({ message: 'No tiene autorizacion' })
    }

    const token = req.headers.authorization.split(" ")[1];

    services.decodeToken(token)
        .then(response => {
            req.user = response
            req.token = token;
            next()
        })
        .catch(({status, message}) => {
            res.status(status).send({message});
        })
}

module.exports = isAuth;