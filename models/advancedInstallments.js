const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const advancedInstallmentsSchema = Schema({
  _idMortgageLoans: { type: String },
  alias: { type: String },
  currentInstallments: { type: String },
  advancedDate: { type: String }
});

module.exports = mongoose.model(
  "advancedInstallments",
  advancedInstallmentsSchema
);
