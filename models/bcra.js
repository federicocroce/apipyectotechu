const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const bcraSchema = Schema({
  _name: { type: String },
  data: { type: Object },
  lastUpdate:{type: String}
});

module.exports = mongoose.model("bcra", bcraSchema);
