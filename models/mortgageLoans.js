const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const mortgageLoansSchema = Schema({
  // _id: false,
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  aliasMortgageLoans: { type: String },
  beginDate: { type: String },
  initialInstallments: { type: String }
});

module.exports = mongoose.model("mortgageLoans", mortgageLoansSchema);
