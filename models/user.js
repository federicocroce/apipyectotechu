"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt-nodejs");
const crypto = require("crypto");
const { createToken } = require("../libs/services");


const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    sparse: true,
    index: true,
  },
  username: String,
  avatar: String,
  password: { type: String, required: true },
  signupDate: { type: Date, default: Date.now() },
  lastLogin: Date,
});

UserSchema.pre("save", function(next) {
  let user = this;
  if (!user.isModified("password")) return next();

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next();

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next();

      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function(candidatePassword) {
  if (!bcrypt.compareSync(candidatePassword, this.password)) {
    throw new Error("Password Incorrecta");
  } else {
    return {
      message: "Te has logueado correctamente",
      token: createToken(this),
    };
  }
};


UserSchema.methods.gravatar = function() {
  if (!this.email) return `https://gravatar.com/avatar/?s=200&d=retro`;

  const md5 = crypto
    .createHash("md5")
    .update(this.email)
    .digest("hex");
  return `https://gravatar.com/avatar/${md5}?s=200&d=retro`;
};

module.exports = mongoose.model("User", UserSchema);
