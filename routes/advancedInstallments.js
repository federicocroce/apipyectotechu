const { getDocuments, findDocuments, saveDocument, deleteDocument, updateDocument } = require('../controllers/advancedInstallments');

const isAuth = require("../middlewares/auth");

const collection = 'advancedInstallments';
const id = 'id'
const _idMortgageLoans = '_idMortgageLoans'
module.exports = app => {

    app.get(`/${collection}`, getDocuments);
    
    app.get(`/${collection}/:${id}`, isAuth, findDocuments);

    app.put(`/${collection}/:${id}`, isAuth, updateDocument);

    app.post(`/${collection}/`, isAuth, saveDocument);

    app.delete(`/${collection}/:${id}/:${_idMortgageLoans}`, isAuth, deleteDocument);
};