const { getDocuments, getDocumentsAxios } = require('../controllers/bcra');

module.exports = app => {
    app.get(`/uva/`, getDocuments("uva"));
    app.get(`/uvaComplete/`, getDocumentsAxios("uva"));

    app.get('/usd', getDocuments("usd"));
    app.get(`/usdComplete/`, getDocumentsAxios("usd"));

};

