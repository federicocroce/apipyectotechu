const { getDocuments, findDocuments, saveDocument, deleteDocument, updateDocument } = require('../controllers/mortgageLoans');

const isAuth = require("../middlewares/auth");

const collection = 'mortgageLoans';
const id = 'id'
module.exports = app => {

    app.get(`/${collection}`, getDocuments);
    
    app.get(`/${collection}/:${id}`, isAuth, findDocuments);

    app.put(`/${collection}/:${id}`, isAuth, updateDocument);

    app.post(`/${collection}`, isAuth, saveDocument);

    app.delete(`/${collection}/:${id}`, isAuth, deleteDocument);
};
