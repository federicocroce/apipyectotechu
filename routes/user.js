const isAuth = require("../middlewares/auth");

const {
  getDocuments,
  signUp,
  signIn,
  deleteDocument
} = require("../controllers/userClass");

module.exports = app => {
  app.get(`/users`, getDocuments);

  app.get(`/private`, (req, res) => {
    console.log(req.user);
    res.status(200).send({
      result: {
        message: "Tiene acceso"
      }
    });
  });

  app.delete(`/users/:id`, deleteDocument);

  app.post(`/signup`, signUp);
  app.post(`/signin`, signIn);
};
